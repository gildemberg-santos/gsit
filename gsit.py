#!/usr/bin/python3
#-*- coding: utf-8 -*-

"""gsit.py: Descrição vai ajusar na clonagem dos repositorios"""

__author__     = "Gildemberg Santos"
__copyright__  = "Copyright 2020, Gildemberg Santos"
__credits__    = ["Gildemberg"]
__license__    = "MIT"
__version__    = "0.0.1"
__maintainer__ = "Gildemberg Santos"
__email__      = "gildemberg.santos@gmail.com"
__status__     = "Production"

import logging
import sys
import os
import argparse


def main():
    # Argumentos
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--repositories', "-r", required=True)
    args = parser.parse_args()
    # Output
    print("https://github.com/gildemberg-santos/{0}".format(args.repositories))
    # Script
    os.system('git clone https://github.com/gildemberg-santos/{0}'.format(args.repositories)) 
    
    return 0

if __name__ == '__main__':
    logging.basicConfig(filename='logs.log', filemode='w', level=logging.DEBUG)
    sys.exit(main())
